package com.thp.spring.dto;

public class RechercheDto {
	
	private Long id_recherche;
	private String typeHabit;
	private String color;
	private  float prix;
	
	public RechercheDto() {
		super();
	}

	
	public RechercheDto(Long id_recherche, String typeHabit, String color, float prix) {
		super();
		this.id_recherche = id_recherche;
		this.typeHabit = typeHabit;
		this.color = color;
		this.prix = prix;
	}


	public Long getId_recherche() {
		return id_recherche;
	}


	public void setId_recherche(Long id_recherche) {
		this.id_recherche = id_recherche;
	}


	public String getTypeHabit() {
		return typeHabit;
	}


	public void setTypeHabit(String typeHabit) {
		this.typeHabit = typeHabit;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public float getPrix() {
		return prix;
	}


	public void setPrix(float prix) {
		this.prix = prix;
	}


	@Override
	public String toString() {
		return "RechercheDto [id_recherche=" + id_recherche + ", typeHabit=" + typeHabit + ", color=" + color
				+ ", prix=" + prix + "]";
	}
	
	
	
	
	
	

}
