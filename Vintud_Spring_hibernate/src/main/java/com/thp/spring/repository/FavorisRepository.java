package com.thp.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.thp.spring.entity.Favoris;

public interface FavorisRepository extends JpaRepository<Favoris, Long> {

}
