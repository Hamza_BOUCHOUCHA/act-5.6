package com.thp.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thp.spring.dto.UserDto;
import com.thp.spring.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping(value = "/addUser")
	public UserDto addUser(@RequestBody UserDto userDto) {
		return userService.addUser(userDto);
	}

	@DeleteMapping(value = "/deleteUser/{id}")
	public Boolean deleteUserById(@PathVariable Long id) {
		return userService.deleteUserById(id);

	}
	
	@GetMapping(value ="/listUser")
	public List<UserDto> getAllUser(){
		return userService.getListUser();
		
	}
	
	@PutMapping(value = "/updateUser")
	public UserDto updateUserById(@RequestBody UserDto userDto) {
		return userService.updateUserById(userDto) ;
		}
	
}
