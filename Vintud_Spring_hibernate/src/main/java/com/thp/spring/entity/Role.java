package com.thp.spring.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "ROLES")
public class Role implements Serializable {
    
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idRole ;
	private String nom ;
	private static final long serialVersionUID = 1L;
	
	
	public Role() {
		super();
	}


	public Role(Long idRole, String nom) {
		super();
		this.idRole = idRole;
		this.nom = nom;
	}


	public Long getIdRole() {
		return idRole;
	}


	public void setIdRole(Long idRole) {
		this.idRole = idRole;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		return "Role [idRole=" + idRole + ", nom=" + nom + "]";
	}

	

	
	
}
