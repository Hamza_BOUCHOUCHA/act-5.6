package com.thp.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.thp.spring.entity.Announcement;

public interface AnnouncementRepository extends JpaRepository<Announcement, Long> {

}
