package com.thp.spring.dto;

import java.sql.Date;
import java.util.Arrays;

public class AnnouncementDto {
	
	private Long idAnnouncement ;
	private String title  ;
	private String description  ;
	private int idCategory  ;
	private float price ;
	private byte[] picture ;
	private Date publicationDate ; 
	private float isAvailable  ;
	private int viewNumber  ;
	private String localisation  ;
	
	public AnnouncementDto() {
		super();
	}

	public AnnouncementDto(Long idAnnouncement, String title, String description, int idCategory, float price,
			byte[] picture, Date publicationDate, float isAvailable, int viewNumber, String localisation) {
		super();
		this.idAnnouncement = idAnnouncement;
		this.title = title;
		this.description = description;
		this.idCategory = idCategory;
		this.price = price;
		this.picture = picture;
		this.publicationDate = publicationDate;
		this.isAvailable = isAvailable;
		this.viewNumber = viewNumber;
		this.localisation = localisation;
	}

	public Long getIdAnnouncement() {
		return idAnnouncement;
	}

	public void setIdAnnouncement(Long idAnnouncement) {
		this.idAnnouncement = idAnnouncement;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getCategory_id() {
		return idCategory;
	}

	public void setCategory_id(int category_id) {
		this.idCategory= idCategory;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public float getIsAvailable() {
		return isAvailable;
	}

	public void setIsavailable(float isAvailable) {
		this.isAvailable = isAvailable;
	}

	public int getViewNumber() {
		return viewNumber;
	}

	public void setView_number(int viewNumber) {
		this.viewNumber = viewNumber;
	}

	public String getLocalisation() {
		return localisation;
	}

	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}

	@Override
	public String toString() {
		return "AnnouncementDto [idAnnouncement=" + idAnnouncement + ", title=" + title + ", description="
				+ description + ", IdCategory=" + idCategory + ", price=" + price + ", picture="
				+ Arrays.toString(picture) + ", publicationDate=" + publicationDate + ", isAvailable=" + isAvailable
				+ ", viewNumber=" + viewNumber + ", localisation=" + localisation + "]";
	}
	
	
	
	
	
	

}
