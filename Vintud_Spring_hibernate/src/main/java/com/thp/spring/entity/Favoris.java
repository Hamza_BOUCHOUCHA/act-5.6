package com.thp.spring.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "FAVORIS")
public class Favoris {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idFavoris;
	private Date dateFav;

	
	public Favoris() {
		super();
	}


	public Favoris(Long idFavoris, Date dateFav) {
		super();
		this.idFavoris = idFavoris;
		this.dateFav = dateFav;
	}


	public Long getIdFavoris() {
		return idFavoris;
	}


	public void setIdFavoris(Long idFavoris) {
		this.idFavoris = idFavoris;
	}


	public Date getDateFav() {
		return dateFav;
	}


	public void setDateFav(Date dateFav) {
		this.dateFav = dateFav;
	}


	@Override
	public String toString() {
		return "Favoris [idFavoris=" + idFavoris + ", dateFav=" + dateFav + "]";
	}
	
	
	
	

	
	
	
	
	
	

}
