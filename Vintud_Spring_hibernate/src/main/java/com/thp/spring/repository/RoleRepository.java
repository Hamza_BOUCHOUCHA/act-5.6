package com.thp.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.thp.spring.entity.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
