package com.thp.spring.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.Arrays;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "announcements")
public class Announcement implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idAnnouncement ;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name ="idUser")
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name ="idCategory")
	private Category category;
	
	private String title  ;
	private String description  ;
	private float price ;
	private byte[] picture ;
	private Date publicationDate ; 
	private float isAvailable  ;
	private int viewNumber  ;
	private String localisation  ;
	
	
	public Announcement() {
		super();
	}


	
	public Announcement(Long idAnnouncement, User user, Category category, String title, String description,
			int idCategory, float price, byte[] picture, Date publicationDate, float isAvailable, int viewNumber,
			String localisation) {
		super();
		this.idAnnouncement = idAnnouncement;
		this.user = user;
		this.category = category;
		this.title = title;
		this.description = description;
	
		this.price = price;
		this.picture = picture;
		this.publicationDate = publicationDate;
		this.isAvailable = isAvailable;
		this.viewNumber = viewNumber;
		this.localisation = localisation;
	}



	public Long getIdAnnouncement() {
		return idAnnouncement;
	}



	public void setIdAnnouncement(Long idAnnouncement) {
		this.idAnnouncement = idAnnouncement;
	}



	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	public Category getCategory() {
		return category;
	}



	public void setCategory(Category category) {
		this.category = category;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	


	public float getPrice() {
		return price;
	}



	public void setPrice(float price) {
		this.price = price;
	}



	public byte[] getPicture() {
		return picture;
	}



	public void setPicture(byte[] picture) {
		this.picture = picture;
	}



	public Date getPublicationDate() {
		return publicationDate;
	}



	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}



	public float getIsAvailable() {
		return isAvailable;
	}



	public void setIsAvailable(float isAvailable) {
		this.isAvailable = isAvailable;
	}



	public int getViewNumber() {
		return viewNumber;
	}



	public void setViewNumber(int viewNumber) {
		this.viewNumber = viewNumber;
	}



	public String getLocalisation() {
		return localisation;
	}



	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public String toString() {
		return "Announcement [idAnnouncement=" + idAnnouncement + ", user=" + user + ", category=" + category
				+ ", title=" + title + ", description=" + description + ", price="
				+ price + ", picture=" + Arrays.toString(picture) + ", publicationDate=" + publicationDate
				+ ", isAvailable=" + isAvailable + ", viewNumber=" + viewNumber + ", localisation=" + localisation
				+ "]";
	}
   
	

	
	
	

}
