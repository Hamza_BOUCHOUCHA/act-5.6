package com.thp.spring.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.core.serializer.Serializer;

@Entity
@Table(name = "`USER`")
public class User implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idUser ;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idRole")
	private Role role;
	
	@OneToOne( fetch = FetchType.LAZY )
	@JoinColumn(name="idRecherche")
	private Recherche recherche;
	
	@OneToOne( fetch = FetchType.LAZY )
	@JoinColumn(name = "idFavoris" )
	private Favoris favoris;

    
	private String firstname ;
	private String name ;
	private String pseudo  ;
	private String mail ;
	private String password  ;
	private String phone ;
	private String address  ;
	
	public User() {
		super();
	}


	public User(Long idUser, Role role, Recherche recherche, Favoris favoris, String firstname, String name,
			String pseudo, String mail, String password, String phone, String address) {
		super();
		this.idUser = idUser;
		this.role = role;
		this.recherche = recherche;
		this.favoris = favoris;
		this.firstname = firstname;
		this.name = name;
		this.pseudo = pseudo;
		this.mail = mail;
		this.password = password;
		this.phone = phone;
		this.address = address;
	}


	public Long getIdUser() {
		return idUser;
	}


	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}


	public Role getRole() {
		return role;
	}


	public void setRole(Role role) {
		this.role = role;
	}


	public Recherche getRecherche() {
		return recherche;
	}


	public void setRecherche(Recherche recherche) {
		this.recherche = recherche;
	}


	public Favoris getFavoris() {
		return favoris;
	}


	public void setFavoris(Favoris favoris) {
		this.favoris = favoris;
	}


	public String getFirstname() {
		return firstname;
	}


	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPseudo() {
		return pseudo;
	}


	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", role=" + role + ", recherche=" + recherche + ", favoris=" + favoris
				+ ", firstname=" + firstname + ", name=" + name + ", pseudo=" + pseudo + ", mail=" + mail
				+ ", password=" + password + ", phone=" + phone + ", address=" + address + "]";
	}
	
	
	
	
	
	
}