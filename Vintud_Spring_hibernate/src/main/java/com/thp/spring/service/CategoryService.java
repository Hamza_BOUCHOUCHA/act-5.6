package com.thp.spring.service;

import java.util.List;

import com.thp.spring.dto.CategoryDto;

public interface CategoryService {
	
	public CategoryDto addCategory (CategoryDto categoryDto) ;
	public Boolean deleteCategroyById (Long id) ;
	public  CategoryDto getCategoryById(Long id) ;
	public CategoryDto updateCategoryById( CategoryDto categoryDto) ;	
	public List<CategoryDto> getListCategory() ;
	
	

}
