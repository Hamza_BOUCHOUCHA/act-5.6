package com.thp.spring.service;

import java.util.List;

import com.thp.spring.dto.CategoryDto;
import com.thp.spring.dto.UserDto;

public interface UserService {
	 
	
	public UserDto addUser (UserDto userDto) ;
	public Boolean deleteUserById (Long id) ;
	public UserDto updateUserById(UserDto userDto);
	public List<UserDto> getListUser() ;
    
}
