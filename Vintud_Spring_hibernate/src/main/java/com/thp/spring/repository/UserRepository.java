package com.thp.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.thp.spring.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
