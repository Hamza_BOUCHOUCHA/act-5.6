package com.thp.spring.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thp.spring.dto.UserDto;
import com.thp.spring.entity.User;
import com.thp.spring.repository.UserRepository;
import com.thp.spring.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private ModelMapper modelMapper = new ModelMapper();

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDto addUser(UserDto userDto) {
		User user = modelMapper.map(userDto, User.class);
		User UserResultat = userRepository.save(user);
		UserDto userDtoResultat = modelMapper.map(UserResultat, UserDto.class);
		return userDtoResultat;
	}

	@Override
	public Boolean deleteUserById(Long id) {
		userRepository.deleteById(id);
		return Boolean.TRUE;
	}

	@Override
	public UserDto updateUserById(UserDto userDto) {
		return addUser(userDto);
	}

	@Override
	public List<UserDto> getListUser() {
		List<User> user = userRepository.findAll();
		List<UserDto> userDto = new ArrayList<UserDto>();

		if (user != null && !user.isEmpty()) {
			for (int i = 0; i < user.size(); i++) {
				userDto.add(modelMapper.map(user.get(i), UserDto.class));
			}
		}
		return userDto;
	}

}
