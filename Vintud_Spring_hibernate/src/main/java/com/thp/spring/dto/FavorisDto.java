package com.thp.spring.dto;

import java.util.Date;

public class FavorisDto {
	
	private Long id_favoris;
	private Date dateFav;
	
	public FavorisDto() {
		super();
	}

	
	public FavorisDto(Long id_favoris, Date dateFav) {
		super();
		this.id_favoris = id_favoris;
		this.dateFav = dateFav;
	}


	public Long getId_favoris() {
		return id_favoris;
	}


	public void setId_favoris(Long id_favoris) {
		this.id_favoris = id_favoris;
	}


	public Date getDateFav() {
		return dateFav;
	}


	public void setDateFav(Date dateFav) {
		this.dateFav = dateFav;
	}


	@Override
	public String toString() {
		return "FavorisDto [id_favoris=" + id_favoris + ", dateFav=" + dateFav + "]";
	}
	
	
	
	
	
	

}
