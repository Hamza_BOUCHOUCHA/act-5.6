package com.thp.spring.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.thp.spring.dto.CategoryDto;
import com.thp.spring.entity.Category;
import com.thp.spring.repository.CategoryRepository;
import com.thp.spring.service.CategoryService;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class CategoryServiceImpl implements CategoryService {

	private ModelMapper modelMapper = new ModelMapper();

	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public CategoryDto addCategory(CategoryDto categoryDto) {
		Category category = modelMapper.map(categoryDto, Category.class);
		Category categoryResultat = categoryRepository.save(category);
		CategoryDto categoryDtoResultat = modelMapper.map(categoryResultat, CategoryDto.class);

		return categoryDtoResultat;
	}

	@Override
	public Boolean deleteCategroyById(Long id) {

		categoryRepository.deleteById(id);

		return Boolean.TRUE;

	}

	@Override
	public CategoryDto getCategoryById(Long id) {
		
		return null;
	}

	@Override
	public CategoryDto updateCategoryById( CategoryDto categoryDto) {
		
		return addCategory(categoryDto);
	}

	@Override
	public List<CategoryDto> getListCategory() {

		List<Category> category = categoryRepository.findAll();
		List<CategoryDto> categoryDto = new ArrayList<CategoryDto>();
		if (category != null && !category.isEmpty()) {
			for (int i = 0; i < category.size(); i++) {
				categoryDto.add(modelMapper.map(category.get(i), CategoryDto.class));

			}
		}

		return categoryDto;
	}
}
