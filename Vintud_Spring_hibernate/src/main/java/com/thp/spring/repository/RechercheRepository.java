package com.thp.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.thp.spring.entity.Recherche;

public interface RechercheRepository extends JpaRepository<Recherche, Long> {

}
