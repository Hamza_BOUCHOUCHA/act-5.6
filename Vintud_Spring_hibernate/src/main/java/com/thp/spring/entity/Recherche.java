package com.thp.spring.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "RECHERCHES")
public class Recherche implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private Long idRecherche;
	private String typeHabit;
	private String color;
	private  float prix;
	
	public Recherche() {
		super();
	}


	public Recherche(Long idRecherche, String typeHabit, String color, float prix) {
		super();
		this.idRecherche = idRecherche;
		this.typeHabit = typeHabit;
		this.color = color;
		this.prix = prix;
	}


	public Long getIdRecherche() {
		return idRecherche;
	}


	public void setIdRecherche(Long idRecherche) {
		this.idRecherche = idRecherche;
	}


	public String getTypeHabit() {
		return typeHabit;
	}


	public void setTypeHabit(String typeHabit) {
		this.typeHabit = typeHabit;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public float getPrix() {
		return prix;
	}


	public void setPrix(float prix) {
		this.prix = prix;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public String toString() {
		return "Recherche [idRecherche=" + idRecherche + ", typeHabit=" + typeHabit + ", color=" + color + ", prix="
				+ prix + "]";
	}

	
	
	

}
