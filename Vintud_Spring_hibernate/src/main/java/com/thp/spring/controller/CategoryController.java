package com.thp.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.thp.spring.dto.CategoryDto;
import com.thp.spring.service.CategoryService;

@RestController
@RequestMapping("/category")
public class CategoryController {

	public CategoryController() {

	}

	@Autowired
	private CategoryService categoryService;

	@PostMapping(value = "/addCategory")
	public CategoryDto addCategory(@RequestBody CategoryDto categoryDto) {
		return categoryService.addCategory(categoryDto);

	}

	@DeleteMapping(value = "/deleteCategory/{id}")
	public Boolean deleteCategory(@PathVariable Long id) {
		return categoryService.deleteCategroyById(id);

	}

	@GetMapping(value = "/listeCategory")
	public List<CategoryDto> getAllCategory() {
			return categoryService.getListCategory();
	
	}
	
	@PutMapping(value = "/updateCategory")
	public CategoryDto updateCategoryById(@RequestBody CategoryDto categoryDto) {
		return categoryService.updateCategoryById( categoryDto);

	}
}