package com.thp.spring.dto;

public class RoleDto {

	private Long id_role ;
	private String nom ;
	
	public RoleDto() {
		super();
	}
	
	public RoleDto(Long id_role, String nom) {
		super();
		this.id_role = id_role;
		this.nom = nom;
	}

	public Long getId_role() {
		return id_role;
	}

	public void setId_role(Long id_role) {
		this.id_role = id_role;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "RoleDto [id_role=" + id_role + ", nom=" + nom + "]";
	}
	
	
	
	
}
