package com.thp.spring.dto;

public class UserDto {
	
	private Long idUser;
	private String firstname ;
	private String name ;
	private String pseudo  ;
	private String mail ;
	private String password  ;
	private String phone ;
	private String address  ;
	
	public UserDto() {
		super();
	}

	public UserDto(Long idUser, String firstname, String name, String pseudo, String mail, String password,
			String phone, String address) {
		super();
		this.idUser = idUser;
		this.firstname = firstname;
		this.name = name;
		this.pseudo = pseudo;
		this.mail = mail;
		this.password = password;
		this.phone = phone;
		this.address = address;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "UserDto [idUser=" + idUser + ", firstname=" + firstname + ", name=" + name + ", pseudo=" + pseudo
				+ ", mail=" + mail + ", password=" + password + ", phone=" + phone + ", address=" + address + "]";
	}

	
	
	
	
}
